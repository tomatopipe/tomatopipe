#!/usr/bin/env python3
"""
Author: Jessica Burger
Script to count the number of reads with feature in the .count files
"""
from sys import argv

input = open(argv[1])

counter = []
for lines in input:
    lines = lines.strip()
    if lines.startswith("gene:"):
        line = lines.split("\t")
        counter.append(int(line[1]))
sum_counter = sum(counter)

print(sum_counter)
