#!/usr/bin/env python3
"""
Author: Beiyu Tu
Script to make a table of CPM for heatmap
"""

import numpy as np


def main(directory):
    filenames = []
    for key in JE01:
        for indexseq in JE01[key]:
            filename1 = 'JE01_' + indexseq + '.count'
            filenames += [filename1]
        for indexseq in JE02[key]:
            filename2 = 'JE02_' + indexseq + '.count'
            filenames += [filename2]
        for indexseq in JE03[key]:
            filename3 = 'JE03_' + indexseq + '.count'
            filenames += [filename3]
    cpmfiles = []
    for filename in filenames:
        filename = directory + '/' + filename
        cpmfile = count_to_cpm(filename)
        cpmfiles += [cpmfile]
    f = 'cpm_' + directory + '.txt'
    e = open(f, 'w')
    e.write('gene')
    for sample in samples:
        e.write('\t' + sample)
    for line in open(cpmfiles[0], 'r'):
        gene = line.split()[0][5:]
        if checkall0(cpmfiles, gene) == False:
            e.write('\n' + gene)
            for cpmfile in cpmfiles:
                for line in open(cpmfile, 'r'):
                    if line.startswith('gene:' + gene):
                        cpm = round(float(line.strip().split()[1]), 2)
                        e.write('\t' + str(cpm))
    e.close()


def count_to_cpm(filename):
    """transform the count file to cpm file

    Key Arguments:
    filename: str, *.count file
    Returns:
    output: str, *.cpm file name
    """
    sum = 0
    for line in open(filename, 'r'):
        if line.startswith('gene:'):
            sum += int(line.strip().split()[1])
    output = filename[:-6] + '.cpm'
    f = open(output, 'w')
    for line in open(filename, 'r'):
        if line.startswith('gene:'):
            gene = line.split()[0]
            count = int(line.strip().split()[1])
            cpm = count / sum * 1000000
            f.write(gene + '\t' + str(cpm) + '\n')
    return output


def checkall0(cpmfiles, gene):
    """check if the cpm of the gene in all samples is 0

    Key Arguments:
    cpmfiles: list
    gene: str
    Returns:
    check: boolean
    """
    check = True
    for cpmfile in cpmfiles:
        for line in open(cpmfile, 'r'):
            if line.startswith('gene:' + gene):
                cpm = float(line.strip().split()[1])
                if cpm != 0:
                    check = False
    return check


if __name__ == '__main__':
    JE01 = {'Mz': ['TTAGGC', 'TGACCA'], 'Chitin': ['CAGATC', 'GGCTAC'], \
            'Sc': ['ACAGTG', 'GCCAAT'], 'Mock1': ['ATCACG', 'CGATGT']}
    JE02 = {'Mz': ['TTAGGC', 'TGACCA'], 'Chitin': ['CAGATC', 'GGCTAC'], \
            'Sc': ['ACAGTG', 'GCCAAT'], 'Mock1': ['ATCACG', 'CGATGT']}
    JE03 = {'Mz': ['ACAGTG', 'GCCAAT'], 'Chitin': ['TTAGGC', 'TGACCA'], \
            'Sc': ['CAGATC', 'GGCTAC'], 'Mock1': ['ATCACG', 'CGATGT']}
    samples = ['Mz_12_1', 'Mz_12_2', 'Mz_24_1', 'Mz_24_2', 'Mz_48_1', 'Mz_48_2', \
               'Chitin_12_1', 'Chitin_12_2', 'Chitin_24_1', 'Chitin_24_2', 'Chitin_48_1', \
               'Chitin_48_2', 'Sc_12_1', 'Sc_12_2', 'Sc_24_1', 'Sc_24_2', 'Sc_48_1', 'Sc_48_2', \
               'Mock1_12_1', 'Mock1_12_2', 'Mock1_24_1', 'Mock1_24_2', 'Mock1_48_1', 'Mock1_48_2']
    main('ITAG2.4')
    main('ITAG4.0')