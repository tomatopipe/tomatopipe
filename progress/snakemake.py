#!/user/bin/env python3
"""
Author: Beiyu Tu
Script to run snakemake with multiple reads
"""

import subprocess
import os.path



def run_snakemake(library, indexseq):
    """run the program snakemake to count the aligned sequence pairs and do gene annotation analysis

     library: str, JE0X
     indexseq: str
     """
    blastxfile = '{}_{}_{}.blast'.format(library, indexseq, pair)
    if not os.path.exists(blastxfile):
        cmd_gff = 'snakemake --cores 2 -s Snakefile_{} {}'.format(library, blastxfile)
        e = subprocess.check_call(cmd_gff, shell=True)


def main():
    indexseqs = ['ATCACG','CGATGT','TTAGGC','TGACCA','ACAGTG','GCCAAT','CAGATC','GGCTAC']
    librarys = ['JE01','JE02','JE03']
    pairs = [1, 2]
    for library in librarys:
        for indexseq in indexseqs:
            for pair in pairs:
                run_snakemake(library, indexseq, pair)

if __name__ == '__main__':
    main()
