1.Run snakemake on the reads using Snakefile_2.4 and Snakefile_4.0 respectively

(Snakefiles were initially written by all; generalized by Stijn Kouwenberg; run by Beiyu Tu, Jessica Burger, David Vos)

input: 
compressed reads files in JE01, JE02, JE03(*.fastq.gz)
genome sequence files from ITAG2.4/ITAG4.0(*.fasta)
genome annotation files from ITAG2.4/ITAG4.0(*.gff)

output:
counts per gene in each sample counted by HTSeq(named as *.count)


2.Run counter.py on the *.count files to count the number of reads with feature.

(counter.py was written and run by Jessica Burger)

input:
counts per gene in each sample counted by HTSeq(named as *.count)

output:
number of reads with feature


3.Extract the number of reads with no feature, ambiguous, too low aQual, not aligned, alignment not unique from the *.count files to create a Excel table, create comparison bar plots and apply paired t-test on the data.

(Done by Jessica Burger)

input: 
the number counted by counter.py and the last five lines of each *.count file

output:
paired t-test result(HTSeq_output_calculated.py)


4.Run cpm_calculate.py on the *.count files to calculate CPM and create a table of CPM(genes with 0 count in every sample deleted).

(cpm_calculate.py was written and run by Beiyu Tu)

input:
counts per gene in each sample counted by HTSeq(named as *.count)

output:
table of CPM with samples as columns and genes as rows(cpm_ITAG2.4.txt, cpm_ITAG4.0.txt)


5.Run pheatmap.R to create heatmaps of normalized CPM

(The initial R Scripts were written by David Vos, Stijn Kouwenberg; The final R script was written by Beiyu Tu)

input:
table of CPM with samples as columns and genes as rows(cpm_ITAG2.4.txt, cpm_ITAG4.0.txt)

output:
heatmaps(pheatmap2.4.png, pheatmap4.0.png)

